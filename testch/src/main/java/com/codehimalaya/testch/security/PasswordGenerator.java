package com.codehimalaya.testch.security;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordGenerator {
    public static void main(String[] args) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        System.out.println(encoder.encode("Dharma"));
        System.out.println(encoder.encode("Kiran"));
        System.out.println(encoder.encode("User"));

    }
}

//password
//$2a$10$swfwb/M6hrocSbdZGXMg.eI0kFTsIy4YGsWzjxzTj5HsKDfKQarmS
//        $2a$10$l.iwDydeEP447h/IaJ9PKOGNSE0ZwH1Or1A9q5NSYnxbCkYa35hYm