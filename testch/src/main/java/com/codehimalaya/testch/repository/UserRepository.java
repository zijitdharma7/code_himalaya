package com.codehimalaya.testch.repository;

import com.codehimalaya.testch.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.CrudRepository;


public interface UserRepository extends CrudRepository<User, Long> {
    User getUserByUsername(String username);

//    @Query("SELECT u from User u Where u.username = :username")
}
