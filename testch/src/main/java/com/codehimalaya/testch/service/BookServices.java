package com.codehimalaya.testch.service;

import com.codehimalaya.testch.model.Book;
import com.codehimalaya.testch.model.User;
import com.codehimalaya.testch.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServices {
    @Autowired
    private BookRepository bookRepository;

    public List<Book> listAll(){
        return bookRepository.findAll();
    }

    public Book get(Long id){
        return bookRepository.findById(id).get();
    }

    public void delete(Long id){
        bookRepository.deleteById(id);
    }


    public void save(Book book) {
        bookRepository.save(book);
    }



}
