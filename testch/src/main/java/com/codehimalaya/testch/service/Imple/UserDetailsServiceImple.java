package com.codehimalaya.testch.service.Imple;

import com.codehimalaya.testch.model.User;
import com.codehimalaya.testch.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;


public class UserDetailsServiceImple implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = this.userRepository.getUserByUsername(username);

        if(user ==null){
            System.out.println("User not found");
            throw new UsernameNotFoundException("Not User Found!!");
        }
        return (UserDetails) user;
    }
}
