package com.codehimalaya.testch.controller;

import com.codehimalaya.testch.model.Book;
import com.codehimalaya.testch.model.User;
import com.codehimalaya.testch.service.BookServices;
import com.codehimalaya.testch.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class AppController {

    @Autowired
    BookServices bookServices;

    UserService userService;

    @RequestMapping("/")
    public String viewHomePage(Model model){
        List<Book> bookList = bookServices.listAll();
        model.addAttribute("bookList", bookList);
        return "index";
    }
//    to create new book
    @RequestMapping("/new")
    public String newBookPage(Model model){
        Book book = new Book();
        model.addAttribute(book);
        return "new_book";
    }
//    post book
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveBook(@ModelAttribute("book") Book book){
        bookServices.save(book);
        return "redirect:/";
    }

//    edit Book
    @GetMapping("edit/{bid}")
    public ModelAndView showEditBookPage(@PathVariable(name = "bid") long bid){
        ModelAndView modelAndView = new ModelAndView("edit_book");
        Book book = bookServices.get(bid);
        modelAndView.addObject("book",book);
        return modelAndView;
    }

//    Delete book
@RequestMapping("delete/{bid}")
    public String deleteBookPage(@PathVariable(name = "bid") long bid){
        bookServices.delete(bid);
        return "redirect:/";
    }

    //-------user------
    @RequestMapping("/admin")
    public String adminHomepage(Model model){
        List<User> bookList = userService.listAll();
        model.addAttribute("userList", bookList);
        return "index";
    }
    //    to create new book
    @RequestMapping("/addUser")
    public String addUser(Model model){
        User user = new User();
        model.addAttribute(user);
        return "new_user";
    }
    //    post book
    @RequestMapping(value = "/post", method = RequestMethod.POST)
    public String postUser(@ModelAttribute("user") User user){
        userService.saveUser(user);
        return "redirect:/admin";
    }

    //    edit Book
    @GetMapping("admin/editUser/{id}")
    public ModelAndView editUser(@PathVariable(name = "id") long id){
        ModelAndView modelAndView = new ModelAndView("edit_user");
        User user = userService.get(id);
        modelAndView.addObject("user",user);
        return modelAndView;
    }

    //    Delete book
    @RequestMapping("deleteUser/{id}")
    public String deleteUser(@PathVariable(name = "id") long id){
        bookServices.delete(id);
        return "redirect:/admin";
    }


}
